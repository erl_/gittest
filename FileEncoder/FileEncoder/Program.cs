﻿using System;
using Encoder;


namespace FileEncoder
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            Console.WriteLine("Введите путь до файла, в который необходимо поместить информацию\r\n");
//            string path = Console.ReadLine();
//            Console.WriteLine("Введите текст, который необходимо поместить в изображение\r\n");
//            string text = Console.ReadLine();
//            using (Encoder.Encoder enc = new Encoder.Encoder(path, text))
//            {
//                enc.Encode();
//            }
            Console.WriteLine("Введите путь к файлу, который нужно расшифровать");
            string pathToDecode = Console.ReadLine();
            using (Encoder.Decoder dec = new Encoder.Decoder(pathToDecode))
            {
                Console.WriteLine(dec.Decode());
            }
        }
    }
}