﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Encoder
{
    public class Encoder : IDisposable
    {
        private string path;
        private string exitPath;
        private string text;
        private FileStream fs;
        private FileStream newFile;
        private List<byte> origbytes;
        private List<byte> newBytes;
        private BinaryReader reader;
        private BinaryWriter writer;
        
        public Encoder(string path, string toEncode)
        {
            this.path = path;
            this.text = toEncode;
            this.exitPath = path + "_encoded.jpeg";
            origbytes = new List<byte>();
            newBytes = new List<byte>();
        }

        public int Encode()
        {
            fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
            newFile = new FileStream(exitPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            reader = new BinaryReader(fs);
            bool flag = false;
            for (int i = 0; i < fs.Length; i++)
            {
                byte _temp = reader.ReadByte();
                origbytes.Add(_temp);
                flag = false;
                if (_temp == 0xFF)
                {
                    flag = true;
                }
                if (_temp == 0xD9 && flag)
                {
                    break;
                }
            }
            newBytes.AddRange(origbytes);
            var _tempstr = TranslateString(text);
            newBytes.AddRange(new byte[]{
                0xFF, 0xEF
            });
            newBytes.AddRange(_tempstr);
            writer = new BinaryWriter(newFile);
            writer.Write(newBytes.ToArray());
            return 1;
        }
        
        private byte[] TranslateString(string toTranslate)
        {
            var s = Encoding.ASCII.GetBytes(toTranslate);
            return s;
        }

        public void Dispose()
        {
            fs?.Flush();
            fs?.Close();
            fs?.Dispose();
            newFile?.Flush();
            newFile?.Close();
            newFile?.Dispose();
        }
    }
}