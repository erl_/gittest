﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Microsoft.SqlServer.Server;

namespace Encoder
{
    public class Decoder : IDisposable
    {
        private string path;
        private BinaryReader reader;
        private FileStream fs;
        private List<byte> readBytes;

        private enum SearchBytes
        {
            BYTE_EOI_INIT = 0xFF,
            BYTE_EOI_END = 0xD9,
            BYTE_MYREG_INIT = 0xFF,
            BYTE_MYREG_END = 0xEF
        }

        public Decoder(string path)
        {
            this.path = path;
            readBytes = new List<byte>();
        }

        public string Decode()
        {
            fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
            reader = new BinaryReader(fs);
            byte last = 0x00;
            bool flag = false;
            bool finish = false;
            bool startRead = false;
            bool go = false;
            for (int i = 0; i < fs.Length; i++)
            {
                byte temp = reader.ReadByte();
                if (!go)
                {
                    if (startRead && temp == 0xEF)
                    {
                        go = true;
                    }
                    else go = false;
                    if (finish && temp == 0xFF)
                    {
                        startRead = true;
                    }
                    else startRead = false;
                    if (flag && temp == 0xD9)
                    {
                        finish = true;
                    }
                    else finish = false;
                    if (temp == 0xFF)
                    {
                        flag = true;
                    }
                    else flag = false;
                }
                else readBytes.Add(temp);
                
            }

            bool dbg = true;
            return TranslateString(readBytes.ToArray());
        }

        private string TranslateString(byte[] bytes)
        {
            var s = Encoding.ASCII.GetString(bytes);
            return s;
        }


        public void Dispose()
        {
            reader?.Dispose();
            fs?.Dispose();
        }
    }
}